package UI;

import Controller.Controller;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.util.Duration;

public class ApplicationUI extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    private Node createSpacer() {
        final Region spacer = new Region();
        // Make it always grow or shrink according to the available space
        VBox.setVgrow(spacer, Priority.ALWAYS);
        return spacer;
    }

    private Node createHSpacer() {
        final Region spacer = new Region();
        // Make it always grow or shrink according to the available space
        HBox.setHgrow(spacer, Priority.ALWAYS);
        return spacer;
    }



    @Override
    public void start(Stage primaryStage) {
        Controller controller = Controller.getInstance();
        primaryStage.setTitle("SantaFactory");
        Button btnSmallGift = new Button();
        Button btnMediumGift = new Button();
        Button btnBigGift = new Button();
        Label label = new Label();
        Label labelLog = new Label();
        Label labelSled = new Label();

        labelSled.setText(controller.getSledCurrentWeight() + "/" + controller.getSledMaxWeight());

        ProgressBar progressBarPacking = new ProgressBar();
        progressBarPacking.setProgress(0.0);

        Button btnDelivery = new Button();
        ProgressBar progressBarDelivery = new ProgressBar();
        progressBarDelivery.setProgress(0.0);

        label.setText("Elf packing : ");
        btnSmallGift.setText("Small gift");
        btnMediumGift.setText("Medium gift");
        btnBigGift.setText("Big gift");
        btnDelivery.setText("Delivery");
        btnSmallGift.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                double preparationPackingTime = controller.packing("small");
                if (preparationPackingTime > 0) {
                    btnSmallGift.setDisable(true);
                    btnMediumGift.setDisable(true);
                    btnBigGift.setDisable(true);
                    Timeline timeline = new Timeline(
                            new KeyFrame(Duration.ZERO, new KeyValue(progressBarPacking.progressProperty(), 0)),
                            new KeyFrame(Duration.seconds(preparationPackingTime), e -> {
                                btnSmallGift.setDisable(false);
                                btnMediumGift.setDisable(false);
                                btnBigGift.setDisable(false);
                                labelLog.setText(controller.getLogSled());
                                labelSled.setText(controller.getSledCurrentWeight() + "/" + controller.getSledMaxWeight());
                            }, new KeyValue(progressBarPacking.progressProperty(), 1))
                    );
                    timeline.setCycleCount(1);
                    timeline.play();
                } else {
                    labelLog.setText(controller.getLogElfPackingFactory());
                }
            }
        });
        btnMediumGift.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                double preparationPackingTime = controller.packing("medium");
                if (preparationPackingTime > 0) {
                    btnSmallGift.setDisable(true);
                    btnMediumGift.setDisable(true);
                    btnBigGift.setDisable(true);
                    Timeline timeline = new Timeline(
                            new KeyFrame(Duration.ZERO, new KeyValue(progressBarPacking.progressProperty(), 0)),
                            new KeyFrame(Duration.seconds(preparationPackingTime), e -> {
                                btnSmallGift.setDisable(false);
                                btnMediumGift.setDisable(false);
                                btnBigGift.setDisable(false);
                                labelLog.setText(controller.getLogSled());
                                labelSled.setText(controller.getSledCurrentWeight() + "/" + controller.getSledMaxWeight());
                            }, new KeyValue(progressBarPacking.progressProperty(), 1))
                    );
                    timeline.setCycleCount(1);
                    timeline.play();
                } else {
                    labelLog.setText(controller.getLogElfPackingFactory());
                }
            }
        });
        btnBigGift.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                double preparationPackingTime = controller.packing("big");
                if (preparationPackingTime > 0) {
                    btnSmallGift.setDisable(true);
                    btnMediumGift.setDisable(true);
                    btnBigGift.setDisable(true);
                    Timeline timeline = new Timeline(
                            new KeyFrame(Duration.ZERO, new KeyValue(progressBarPacking.progressProperty(), 0)),
                            new KeyFrame(Duration.seconds(preparationPackingTime), e -> {
                                btnSmallGift.setDisable(false);
                                btnMediumGift.setDisable(false);
                                btnBigGift.setDisable(false);
                                labelLog.setText(controller.getLogSled());
                                labelSled.setText(controller.getSledCurrentWeight() + "/" + controller.getSledMaxWeight());
                            }, new KeyValue(progressBarPacking.progressProperty(), 1))
                    );
                    timeline.setCycleCount(1);
                    timeline.play();
                } else {
                    labelLog.setText(controller.getLogElfPackingFactory());
                }
            }
        });
        btnDelivery.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                double deloadTime = controller.delivery();
                if (deloadTime > 0) {
                    btnSmallGift.setDisable(true);
                    btnMediumGift.setDisable(true);
                    btnBigGift.setDisable(true);
                    btnDelivery.setDisable(true);
                    Timeline timeline = new Timeline(
                            new KeyFrame(Duration.ZERO, new KeyValue(progressBarDelivery.progressProperty(), 0)),
                            new KeyFrame(Duration.seconds(deloadTime), e -> {
                                btnSmallGift.setDisable(false);
                                btnMediumGift.setDisable(false);
                                btnBigGift.setDisable(false);
                                btnDelivery.setDisable(false);
                                labelLog.setText(controller.getLogSled());
                                labelSled.setText(controller.getSledCurrentWeight() + "/" + controller.getSledMaxWeight());
                            }, new KeyValue(progressBarDelivery.progressProperty(), 1))
                    );
                    timeline.setCycleCount(1);
                    timeline.play();
                } else {
                    labelLog.setText(controller.getLogSled());
                }
            }
        });

        VBox root = new VBox(10);
        HBox progressPacking = new HBox();
        HBox packing = new HBox();

        HBox progressDelivery = new HBox();
        HBox delivery = new HBox();

        HBox displayLog = new HBox();
        displayLog.setAlignment(Pos.CENTER);

        root.setAlignment(Pos.CENTER);

        packing.setAlignment(Pos.CENTER);
        packing.getChildren().addAll(label, btnSmallGift, btnMediumGift, btnBigGift);
        progressBarPacking.prefWidthProperty().bind(root.widthProperty().multiply(0.9));
        progressPacking.setAlignment(Pos.CENTER);
        progressPacking.getChildren().addAll(progressBarPacking);

        delivery.setAlignment(Pos.CENTER);
        delivery.getChildren().addAll(createHSpacer(), labelSled, createHSpacer(), btnDelivery, createHSpacer());
        progressBarDelivery.prefWidthProperty().bind(root.widthProperty().multiply(0.9));
        progressDelivery.setAlignment(Pos.CENTER);
        progressDelivery.getChildren().addAll(progressBarDelivery);

        root.setFillWidth(true);
        root.getChildren().addAll(createSpacer(), progressPacking, createSpacer(), packing,createSpacer(), progressDelivery, createSpacer(), delivery, createSpacer(), labelLog, createSpacer());
        primaryStage.setScene(new Scene(root, 400, 350));
        primaryStage.show();
    }
}