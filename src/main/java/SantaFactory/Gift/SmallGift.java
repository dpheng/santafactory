package SantaFactory.Gift;

public class SmallGift extends Gift {
    public SmallGift() {
        super("small", 1, 0.5);
    }
}
