package SantaFactory.Gift;

public abstract class Gift {
    private String type;
    private int weight;
    private double preparationTime;

    public Gift(String type, int weight, double preparationTime) {
        this.type = type;
        this.weight = weight;
        this.preparationTime = preparationTime;
    }

    public String getType() {
        return type;
    }

    public int getWeight() {
        return weight;
    }

    public double getPreparationTime() {
        return preparationTime;
    }
}
