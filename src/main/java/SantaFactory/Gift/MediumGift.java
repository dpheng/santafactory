package SantaFactory.Gift;

public class MediumGift extends Gift {
    public MediumGift() {
        super("medium", 2, 1.0);
    }
}
