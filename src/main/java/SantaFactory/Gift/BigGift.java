package SantaFactory.Gift;

public class BigGift extends Gift {
    public BigGift() {
        super("big", 5, 2.0);
    }
}
