package SantaFactory;

import java.util.Scanner;

public class SantaFactory {
    static public void main(String[] args) {
        ElfPackingFactory elfPackingFactory = new ElfPackingFactory();
        Sled sled = new Sled(12, 0.5);

        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.println("1. create small gift");
            System.out.println("2. create medium gift");
            System.out.println("3. create big gift");
            System.out.println("4. gift delivery");
            System.out.println("5. exit");
            String cmd = sc.next();
            System.out.println("");
            switch (cmd) {
                case "1":
                    sled.load(elfPackingFactory.elfPackingGift("small", sled.getCurrentWeight(), sled.getMaxWeight()));
                    break;
                case "2":
                    sled.load(elfPackingFactory.elfPackingGift("medium", sled.getCurrentWeight(), sled.getMaxWeight()));
                    break;
                case "3":
                    sled.load(elfPackingFactory.elfPackingGift("big", sled.getCurrentWeight(), sled.getMaxWeight()));
                    break;
                case "4":
                    sled.deload();
                    break;
                case "5":
                    System.exit(0);
                    break;
            }
        }
    }
}
