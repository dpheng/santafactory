package SantaFactory;

import SantaFactory.Gift.Gift;

import java.util.ArrayList;
import java.util.Random;

public class Sled {
    private boolean UI = false;
    private int maxWeight;
    private int currentWeight;
    private double deloadTime;
    private ArrayList<Gift> inventory = new ArrayList<>();
    private String log;

    public Sled(int maxWeight, double deloadTime, boolean UI) {
        this.maxWeight = maxWeight;
        this.deloadTime = deloadTime;
        this.currentWeight = 0;
        this.UI = UI;
    }


    public Sled(int maxWeight, double deloadTime) {
        this.maxWeight = maxWeight;
        this.deloadTime = deloadTime;
        this.currentWeight = 0;
    }

    public boolean load(Gift newGift) {
        if (newGift == null)
            return false;
        log = newGift.getType() + " added !";
        System.out.println(log);
        inventory.add(newGift);
        currentWeight += newGift.getWeight();
        System.out.println("The sled current weight is  " + currentWeight + "/" + maxWeight);
        return true;
    }

    protected int generateRand(int range) {
        Random randomGenerator = new Random();
        return randomGenerator.nextInt(range) + 1;
    }

    public boolean deload() {
        if (inventory.isEmpty()) {
            log = "Nothing to deliever";
            System.out.println(log);
            return false;
        }
        boolean isReindeerHungry = generateRand(5) == 1 ? true : false;

        if (isReindeerHungry) {
            log = "Reindeer hungry ! :O";
            System.out.println(log);
            return false;
        }
        for (Gift gift : inventory) {
            try {
                if (!UI)
                    Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Deload " + gift.getType() + " gift during " + this.deloadTime);
        }
        inventory.clear();
        currentWeight = 0;
        log = "Delievery done !";
        return true;
    }

    public int getMaxWeight() {
        return maxWeight;
    }

    public int getCurrentWeight() {
        return currentWeight;
    }

    public ArrayList<Gift> getInventory() {
        return inventory;
    }

    public double getDeloadTime() {
        return deloadTime;
    }

    public String getLog() {
        return log;
    }
}
