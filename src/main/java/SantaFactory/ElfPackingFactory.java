package SantaFactory;

import SantaFactory.Gift.*;

public class ElfPackingFactory {
    private boolean UI = false;
    private String log;

    public ElfPackingFactory() {

    }

    public ElfPackingFactory(boolean UI) {
        this.UI = UI;
    }

    public Gift elfPackingGift(String giftType, int sledWeight, int sledMaxWeight) {
        Gift gift;

        switch (giftType) {
            case "big":
                gift =  new BigGift();
                break;
            case "medium":
                gift =  new MediumGift();
                break;
            case "small":
                gift = new SmallGift();
                break;
            default:
                gift = null;
        }
        if (gift == null) {
            log = "This gift type doesn't exist";
            System.out.println(log);
        } else if (sledWeight + gift.getWeight() > sledMaxWeight) {
            log = "Don't want to work, sled is already full";
            System.out.println(log);
            return null;
        } else {
            try {
                if (!UI)
                    Thread.sleep((long) gift.getPreparationTime() * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return gift;
    }

    public String getLog() {
        return log;
    }
}
