package Controller;

import SantaFactory.ElfPackingFactory;
import SantaFactory.Gift.Gift;
import SantaFactory.Sled;

import java.util.ArrayList;

public class Controller {
    private ElfPackingFactory elfPackingFactory = null;
    private Sled sled = null;
    private static Controller controller = null;

    private Controller() {
        elfPackingFactory = new ElfPackingFactory(true);
        sled = new Sled(12, 0.5, true);
    }

    public static Controller getInstance() {
        if (controller == null)
            controller = new Controller();
        return controller;
    }

    public double packing(String giftType) {
        if (sled.load(elfPackingFactory.elfPackingGift(giftType, sled.getCurrentWeight(), sled.getMaxWeight()))) {
            ArrayList<Gift> inventory = sled.getInventory();
            return inventory.get(inventory.size() - 1).getPreparationTime();
        }
        return -1;
    }

    public double delivery() {
        int nbGift = sled.getInventory().size();
        if (sled.deload())
            return nbGift * sled.getDeloadTime();
        return -1;
    }

    public String getLogSled() {
        return sled.getLog();
    }

    public String getLogElfPackingFactory() {
        return elfPackingFactory.getLog();
    }

    public int getSledCurrentWeight() {
        return sled.getCurrentWeight();
    }

    public int getSledMaxWeight() {
        return sled.getMaxWeight();
    }
}
