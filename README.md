# SantaFactory

SantaFactory application in command line.
Require JAVA 8 or more.
Require JAVA FX 8.0.66-b17 or more.

Installation:
    
    cd SantaFactory/
    mvn clean install
    
Run in console:

    cd SantaFactory/target/
    java -cp SantaFactory-1.0-SNAPSHOT.jar SantaFactory.SantaFactory
    
Run with UI:

    cd SantaFactory/target/
    java -cp SantaFactory-1.0-SNAPSHOT.jar UI.ApplicationUI